//
//  StartViewController.swift
//  Task-7
//
//  Created by Александр Савков on 15.03.22.
//

import UIKit

class StartViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .cyan
    }
}
