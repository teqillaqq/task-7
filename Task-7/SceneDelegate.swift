//
//  SceneDelegate.swift
//  Task-7
//
//  Created by Александр Савков on 15.03.22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        let startVC = StartViewController()
        let startNC = UINavigationController(rootViewController: startVC)
        window?.rootViewController = startNC
        window?.makeKeyAndVisible()
    }
}

